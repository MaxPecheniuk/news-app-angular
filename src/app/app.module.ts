import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthMenuComponent } from './components/auth-menu/auth-menu.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSidenavModule, MatToolbarModule, MatListModule, MatProgressSpinnerModule, MatTreeModule, MatTooltipModule
} from '@angular/material';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { firebaseConfig } from './config/firebaseConfig';
import { FirebaseService } from './core/services/firebase.service';
import { NewsCardComponent } from './components/news-card/news-card.component';
import { SubmitFormComponent } from './components/submit-form/submit-form.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { MatMenuModule } from '@angular/material/menu';
import { DeleteNewsItemDialogComponent } from './components/news-card/delete-news-item-dialog/delete-news-item-dialog.component';
import { FormsModule } from '@angular/forms';
import { ControlPanelComponent } from './components/control-panel/control-panel.component';
import { SortingComponent } from './components/sorting/sorting.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LayoutModule } from '@angular/cdk/layout';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { AddButtonComponent } from './components/add-button/add-button.component';
import { HomeService } from './components/home/service/home.service';
import { AuthService } from './core/services/auth.service';
import { ControlPanelService } from './components/control-panel/service/control-panel.service';
import { SearchFormComponent } from './components/search-form/search-form.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthMenuComponent,
    SubmitFormComponent,
    NewsCardComponent,
    SubmitFormComponent,
    HomeComponent,
    NotFoundComponent,
    DeleteNewsItemDialogComponent,
    ControlPanelComponent,
    SortingComponent,
    SideNavComponent,
    AddButtonComponent,
    SearchFormComponent,
  ],
  entryComponents: [
    SubmitFormComponent,
    DeleteNewsItemDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(firebaseConfig),
    MatDialogModule,
    MatIconModule,
    MatDividerModule,
    MatMenuModule,
    FormsModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatPaginatorModule,
    LayoutModule,
    MatToolbarModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatTreeModule,
    MatTooltipModule
  ],

  providers: [MatDatepickerModule, FirebaseService, AuthService, HomeService, ControlPanelService],
  bootstrap: [AppComponent]
})
export class AppModule {
}

