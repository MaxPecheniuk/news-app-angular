import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AuthModel } from '../../models/authModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authData: AuthModel = null;

  constructor(private afAuth: AngularFireAuth,
              private db: AngularFireDatabase,
              private router: Router) {
    this.afAuth.authState.subscribe((auth) => {
      this.authData = auth;
    });

  }

  // Returns true if user is logged in
  get authenticated(): boolean {
    return this.authData !== null;
  }

  // Returns current user data
  get currentUser() {
    if (this.authenticated) {
      return this.authData;
    }
  }

  // Returns current user UID
  get currentUserId(): string {
    if (this.authenticated) {
      return this.authData.uid;
    }
  }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.socialSignIn(provider);
  }

  private socialSignIn(provider) {
    this.afAuth.auth.signInWithPopup(provider)
      .then((authData) => {
        this.authData = authData.user;
      })
      .catch((error) => {
        throw new Error(error);
      });
  }

  signOut(): void {
    this.afAuth.auth.signOut();
    this.router.navigateByUrl('/home');

  }
}
