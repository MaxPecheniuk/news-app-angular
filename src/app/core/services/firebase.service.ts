import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { NewsModel } from '../../models/news.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  private postRef: AngularFireList<NewsModel | any> = this.db.list('/news');
  private newsData: Observable<Array<NewsModel>>;

  constructor(private db: AngularFireDatabase, private authDB: AuthService) {
  }

  // Return all news
  getAllData() {
    return this.newsData = this.snapshotData(this.postRef);
  }

  // Return news with status true
  getValidData() {
    this.postRef.query.orderByChild('status').equalTo(true);
    return this.newsData = this.snapshotData(this.postRef);
  }

  // Return news by user ID
  getOwnData() {
    this.postRef.query.orderByChild('authorID').equalTo(this.authDB.currentUserId);
    return this.newsData = this.snapshotData(this.postRef);

  }

  private snapshotData(data: AngularFireList<NewsModel>) {
    return this.newsData = data.snapshotChanges().pipe(
      map(changes => {
        return changes.map(c => ({
          key: c.payload.key, ...c.payload.val()
        }));
      })
    );
  }

  public postData(post) {
    this.postRef.push(post);
  }

  public updateData(key: string, value: object) {
    this.postRef.update(key, value);
  }

  public deletePost(post) {
    this.postRef.remove(post);
  }
}
