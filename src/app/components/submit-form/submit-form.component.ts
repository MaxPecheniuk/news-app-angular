import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from '../../core/services/firebase.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AuthService } from '../../core/services/auth.service';
import { NewsModel } from '../../models/news.model';


@Component({
  selector: 'app-submit-form',
  templateUrl: './submit-form.component.html',
  styleUrls: ['./submit-form.component.scss']
})
export class SubmitFormComponent implements OnInit {
  private startDate: string;
  private endDate: string;
  
  submitForm = new FormGroup({
    type: new FormControl('', Validators.required),
    text: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required),
    startDate: new FormControl('', Validators.required),
    endDate: new FormControl('', Validators.required),
    status: new FormControl(false),
    authorName: new FormControl(this.auth.currentUser.displayName),
    authorID: new FormControl(this.auth.currentUserId)

  });

  constructor(
    private newsDB: FirebaseService,
    private auth: AuthService,
    private dialogRef: MatDialogRef<SubmitFormComponent>,
    @Inject(MAT_DIALOG_DATA) private newsData: NewsModel) {
  }

  ngOnInit() {
    if (this.newsData !== null) {
      this.submitForm = new FormGroup({
        type: new FormControl(this.newsData.type, Validators.required),
        text: new FormControl(this.newsData.text, Validators.required),
        title: new FormControl(this.newsData.title, Validators.required),
        startDate: new FormControl(this.newsData.startDate, Validators.required),
        endDate: new FormControl(this.newsData.endDate, Validators.required),
        status: new FormControl(this.newsData.status),
        authorName: new FormControl(this.auth.currentUser.displayName),
        authorID: new FormControl(this.auth.currentUserId)
      });
    }
  }

  filterStartDate = (date: Date): any => {
    this.startDate = date.toISOString();
    return this.startDate;
  }

  filterEndDate = (date: Date): any => {
    this.endDate = date.toISOString();
    return this.endDate;
  }

  onSubmit() {
    this.submitForm.patchValue({startDate: this.startDate});
    this.submitForm.patchValue({endDate: this.endDate});
    if (this.newsData !== null) {
      this.newsDB.updateData(this.newsData.key, this.submitForm.value);
    } else {
      this.newsDB.postData(this.submitForm.value);
    }
    this.closeDialog();
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
