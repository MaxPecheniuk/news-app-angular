import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { SubmitFormComponent } from '../submit-form/submit-form.component';

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.scss']
})
export class AddButtonComponent implements OnInit {

  constructor(private dialog: MatDialog, private auth: AuthService, public router: Router) { }

  ngOnInit() {
  }

  openDialog() {
    this.dialog.open(SubmitFormComponent, {
      width: '350px',
    });
  }
}
