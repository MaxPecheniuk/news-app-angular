import { Component, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

export interface Search {
  searchValue: string;
  searchType: string;
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})

export class SearchFormComponent implements OnInit {

  @Output() searchNewsValue = new EventEmitter<Search>();
  showSearchField: boolean;
  selectedTypeOfSearch: string = 'text';

  constructor(public router: Router) {
  }

  filterForm = new FormGroup({
    selectInput: new FormControl('')
  });

  @HostListener('window:resize')
  onResize() {
    if (
      this.router.url === ('/control_panel') && window.innerWidth >= 1300 ||
      this.router.url === ('/home') && window.innerWidth >= 1000) {
      return this.showSearchField = true;
    } else {
      return this.showSearchField = false;
    }
  }

  ngOnInit() {
    this.onResize();
  }

  onKey(inputValue: string) {
    const searchData: Search = {
      searchValue: inputValue,
      searchType: this.selectedTypeOfSearch
    }
    this.searchNewsValue.emit(searchData);

  }
}
