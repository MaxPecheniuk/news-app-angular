import { Injectable } from '@angular/core';
import { NewsModel } from '../../../models/news.model';
import { FirebaseService } from '../../../core/services/firebase.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private currentData: string = new Date().toISOString();
  private limitOfNews: number = 4;
  newsData: Array<NewsModel> = [];

  constructor(private newsDB: FirebaseService) {
  }

  init() {
    this.fetchPosts();
  }

  fetchPosts() {

    this.newsDB.getValidData().subscribe((news: Array<NewsModel>) => {
      this.newsData = news.filter((item: NewsModel) => {
        return item.startDate <= this.currentData && item.endDate >= this.currentData;
      });
      this.sortingPosts();
    });
  }

  sortingPosts() {
    this.newsData.sort((a, b) => {
        if (a.startDate < b.startDate) {
          return 1;
        }
        if (a.startDate > b.startDate) {
          return -1;
        }
      }
    );
    this.limitData(this.limitOfNews);
    return this.newsData;
  }

  limitData(lastEl: number) {
    this.newsData.splice(lastEl);
  }

  searchValue(inputValue: string) {
    if (inputValue !== '') {
      console.log(inputValue);
      this.newsDB.getValidData().subscribe((news: Array<NewsModel>) => {
        return this.newsData = news.filter((newsItem: NewsModel) => newsItem.text.includes(inputValue));
      });
    } else {
      this.fetchPosts();
    }
  }

  changeLimit() {
    this.limitOfNews += 4;
    this.fetchPosts();
  }
}
