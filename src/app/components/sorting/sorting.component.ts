import { Component, HostListener, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NewsModel } from '../../models/news.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sorting',
  templateUrl: './sorting.component.html',
  styleUrls: ['./sorting.component.scss']
})
export class SortingComponent implements OnInit {

  @Input() newsData: Array<NewsModel>;
  showFilter: boolean;

  constructor(public router: Router) {
  }

  filterForm = new FormGroup({
    selectInput: new FormControl('')
  });

  @HostListener('window:resize')
  onResize() {
    if (
      this.router.url === ('/control_panel') && window.innerWidth >= 1300 ||
      this.router.url === ('/home') && window.innerWidth >= 1000) {
      return this.showFilter = true;
    } else {
      return this.showFilter = false;
    }
  }

  ngOnInit() {
    this.onResize();
  }

  sorting(value?: string) {
    if (value === 'Descending') {
      this.newsData.sort((a, b) => {
          if (a.startDate < b.startDate) {
            return 1;
          }
          if (a.startDate > b.startDate) {
            return -1;
          }
        }
      );
    }
    if (value === 'Ascending') {
      this.newsData.sort((a, b) => {
          if (a.startDate > b.startDate) {
            return 1;
          }
          if (a.startDate < b.startDate) {
            return -1;
          }
        }
      );
    }
  }
}
