import { Injectable } from '@angular/core';
import { NewsModel } from '../../../models/news.model';
import { FirebaseService } from '../../../core/services/firebase.service';
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';
import { adminUID } from '../../../config/adminUID';
import { Search } from '../../search-form/search-form.component';

@Injectable({
  providedIn: 'root'
})
export class ControlPanelService {

  private currentData: string = new Date().toISOString();
  newsData: Array<NewsModel>;

  constructor(private newsDB: FirebaseService, public auth: AuthService, private router: Router) {
  }

  init() {
    if (this.auth.authenticated && this.auth.currentUserId !== adminUID) {
      this.fetchPostsAsUser();
    } else if (this.auth.currentUserId === adminUID) {
      this.fetchPostsAsAdmin();
    } else {
      this.router.navigateByUrl('/404');
    }
  }

  fetchPostsAsAdmin() {
    this.newsDB.getAllData().subscribe((news: Array<NewsModel>) => {
      this.newsData = news;
      this.sortingPosts();
    });
  }

  fetchPostsAsUser() {
    this.newsDB.getAllData().subscribe((news: Array<NewsModel>) => {
      this.newsData = news.filter((item: NewsModel) => {
        return item.authorID === this.auth.currentUserId ||
          item.status === true &&
          item.startDate <= this.currentData && item.endDate >= this.currentData;
      });
      this.sortingPosts();

    });
  }

  fetchOwnNews() {
    this.newsDB.getOwnData().subscribe((news: Array<NewsModel>) => {
      return this.newsData = news;
    });
  }

  sortingPosts(): Array<NewsModel> {
    this.newsData.sort((a, b) => {
        if (a.startDate < b.startDate) {
          return 1;
        }
        if (a.startDate > b.startDate) {
          return -1;
        }
      }
    );
    return this.newsData;
  }

  searchValue(inputValue: Search) {
    if (inputValue.searchValue !== '') {
      if (inputValue.searchType === 'text') {
        this.searchByText(inputValue.searchValue);
      }
      if (inputValue.searchType === 'title') {
        this.searchByTitle(inputValue.searchValue);
      }
      if (inputValue.searchType === 'author') {
        this.searchByAuthorName(inputValue.searchValue);
      }
    } else {
      this.fetchPostsAsUser();
    }
  }

  searchByTitle(inputValue: string) {
    this.newsDB.getAllData().subscribe((news: Array<NewsModel>) => {
      return this.newsData = news.filter((newsItem: NewsModel) => newsItem.title.includes(inputValue));
    });
  }

  searchByText(inputValue: string) {
    this.newsDB.getAllData().subscribe((news: Array<NewsModel>) => {
      return this.newsData = news.filter((newsItem: NewsModel) => newsItem.text.includes(inputValue));
    });
  }

  searchByAuthorName(inputValue: string) {
    this.newsDB.getAllData().subscribe((news: Array<NewsModel>) => {
      return this.newsData = news.filter((newsItem: NewsModel) => newsItem.authorName.includes(inputValue));
    });
  }
}
