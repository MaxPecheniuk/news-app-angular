import { Component, OnInit } from '@angular/core';
import { ControlPanelService } from './service/control-panel.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent implements OnInit {

  constructor(public serviceControlPanel: ControlPanelService) {
  }

  ngOnInit() {
    this.serviceControlPanel.init();
  }
}
