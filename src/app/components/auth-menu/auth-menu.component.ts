import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-auth-menu',
  templateUrl: './auth-menu.component.html',
  styleUrls: ['./auth-menu.component.scss']
})
export class AuthMenuComponent implements OnInit {

  constructor(public auth: AuthService) {
  }

  ngOnInit() {
  }

  signIn() {
    this.auth.googleLogin();

  }

}
