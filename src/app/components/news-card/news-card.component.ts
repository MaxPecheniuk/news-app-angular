import { Component, Input, OnInit } from '@angular/core';
import { NewsModel } from '../../models/news.model';
import { Router } from '@angular/router';
import { FirebaseService } from '../../core/services/firebase.service';
import { MatDialog } from '@angular/material';
import { DeleteNewsItemDialogComponent } from './delete-news-item-dialog/delete-news-item-dialog.component';
import { AuthService } from '../../core/services/auth.service';
import { SubmitFormComponent } from '../submit-form/submit-form.component';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent implements OnInit {

  @Input() newsItem: NewsModel;

  constructor(
    private dialog: MatDialog,
    public router: Router,
    private newsDB: FirebaseService,
    private auth: AuthService) {
  }

  ngOnInit() {
  }

  openDeleteDialog(key) {
    this.dialog.open(DeleteNewsItemDialogComponent, {data: key});
  }

  openEditDialog(data: NewsModel) {
    this.dialog.open(SubmitFormComponent, {width: '350px', data});
  }

  updateNews = (key: string, value: boolean) => {
    this.newsDB.updateData(key, {'status': value});
  }
}
