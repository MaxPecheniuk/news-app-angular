import { Component, Inject, Input, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { FirebaseService } from '../../../core/services/firebase.service';

@Component({
  selector: 'app-delete-news-item-dialog',
  templateUrl: './delete-news-item-dialog.component.html',
  styleUrls: ['./delete-news-item-dialog.component.scss']
})
export class DeleteNewsItemDialogComponent implements OnInit {


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string,
    private dialogRef: MatDialogRef<DeleteNewsItemDialogComponent>,
    private newsDB: FirebaseService,
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  deleteNewsItem(key: string, message: string, action?: string) {
    this.newsDB.deletePost(key);
    this.closeDialog();
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
