import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteNewsItemDialogComponent } from './delete-news-item-dialog.component';

describe('DeleteNewsItemDialogComponent', () => {
  let component: DeleteNewsItemDialogComponent;
  let fixture: ComponentFixture<DeleteNewsItemDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteNewsItemDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteNewsItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
