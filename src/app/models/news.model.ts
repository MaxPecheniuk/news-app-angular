export interface NewsModel {
  key: string;
  title: string;
  type: string;
  text: string;
  startDate: string;
  endDate: string;
  status: boolean;
  authorName: string;
  authorID: string;
}
