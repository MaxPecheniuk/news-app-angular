export interface AuthModel {
  displayName: string;
  email: string;
  photoURL: string;
  uid: string;
}
